# -*- coding: utf-8 -*-
# noinspection PyUnboundLocalVariable
__path__ = __import__('pkgutil').extend_path(__path__, __name__)

__copyright__ = "Copyright 2018, The Reseller Gateway oy"
__license__ = "Commercial"
