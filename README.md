# Empty project template for new APIs

After copying or merging this into your project repo

1. move Pipfile to some other name (this is due to pipenv currently being broken)
1. run pipenv install
1. rename directory `gwio/python_package` directory to package name
1. edit tests/test_base.py to import your package
1. edit setup.py - fix the package name and add your runtime requirements
1. run pipenv shell and in that shell run pip install -e . then exit the shell
1. move Pipfile back
1. edit Pipfile (add your devtime requirements (if they are not covered by the devenv))
1. edit `zappa_settings.json` - fix the package name.
1. update this README.md to describe your project
1. run pipenv update --dev --sequential
1. code (add test - write code - rinse and repeat)
1. ???
1. Profit!
