import datetime

from setuptools import setup, find_packages


def make_calver():
    n = datetime.datetime.utcnow()
    d = datetime.datetime.utcnow().replace(hour=0, day=1, minute=0, second=0)
    dt = (n - d)
    return '{n.year}.{n.month}.{release}'.format(n=n, release=int(dt.total_seconds() / 60))


install_requires = [
        '<YOUR-RUNTIME-REQUIREMENTS-GO-HERE>'
]

setup(name='gwio-your-api-name',
      version=make_calver(),
      description='',
      url='https://the.gw/',
      author='The Reseller Gateway Oy',
      author_email='dev@the.gw',
      platforms=['POSIX'],
      packages=find_packages(),
      package_data={},
      include_package_data=False,
      install_requires=install_requires,
      zip_safe=False)
